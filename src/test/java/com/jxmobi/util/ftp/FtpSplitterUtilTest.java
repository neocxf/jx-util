package com.jxmobi.util.ftp;

import com.jxmobi.util.algorithm.AlgorithmUtils;
import com.jxmobi.util.file.FileSplitterUtil;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.misc.CRC16;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.Comparator;
import java.util.stream.Stream;
import java.util.zip.CRC32;

import static com.jxmobi.util.file.FileSplitterUtil.read;
import static com.jxmobi.util.file.FileSplitterUtil.readAllFileStream;

/**
 * @author Xiaofei Chen <a href="mailto:xchen@jxmobi.com">Email the author</a>
 * @version 1.0 5/28/16
 */
public class FtpSplitterUtilTest {
    private static final Logger log = LoggerFactory.getLogger(FtpSplitterUtilTest.class);

    @Test
    public void testCrc16() throws UnsupportedEncodingException {
        crc16("hello".getBytes("utf-8"));

        System.out.println(Integer.toHexString(AlgorithmUtils.crc16("hello".getBytes("utf-8"))));
    }

    public int crc16Algorothim(byte[] data) {
        int[] table = {
                0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301, 0x03C0, 0x0280, 0xC241,
                0xC601, 0x06C0, 0x0780, 0xC741, 0x0500, 0xC5C1, 0xC481, 0x0440,
                0xCC01, 0x0CC0, 0x0D80, 0xCD41, 0x0F00, 0xCFC1, 0xCE81, 0x0E40,
                0x0A00, 0xCAC1, 0xCB81, 0x0B40, 0xC901, 0x09C0, 0x0880, 0xC841,
                0xD801, 0x18C0, 0x1980, 0xD941, 0x1B00, 0xDBC1, 0xDA81, 0x1A40,
                0x1E00, 0xDEC1, 0xDF81, 0x1F40, 0xDD01, 0x1DC0, 0x1C80, 0xDC41,
                0x1400, 0xD4C1, 0xD581, 0x1540, 0xD701, 0x17C0, 0x1680, 0xD641,
                0xD201, 0x12C0, 0x1380, 0xD341, 0x1100, 0xD1C1, 0xD081, 0x1040,
                0xF001, 0x30C0, 0x3180, 0xF141, 0x3300, 0xF3C1, 0xF281, 0x3240,
                0x3600, 0xF6C1, 0xF781, 0x3740, 0xF501, 0x35C0, 0x3480, 0xF441,
                0x3C00, 0xFCC1, 0xFD81, 0x3D40, 0xFF01, 0x3FC0, 0x3E80, 0xFE41,
                0xFA01, 0x3AC0, 0x3B80, 0xFB41, 0x3900, 0xF9C1, 0xF881, 0x3840,
                0x2800, 0xE8C1, 0xE981, 0x2940, 0xEB01, 0x2BC0, 0x2A80, 0xEA41,
                0xEE01, 0x2EC0, 0x2F80, 0xEF41, 0x2D00, 0xEDC1, 0xEC81, 0x2C40,
                0xE401, 0x24C0, 0x2580, 0xE541, 0x2700, 0xE7C1, 0xE681, 0x2640,
                0x2200, 0xE2C1, 0xE381, 0x2340, 0xE101, 0x21C0, 0x2080, 0xE041,
                0xA001, 0x60C0, 0x6180, 0xA141, 0x6300, 0xA3C1, 0xA281, 0x6240,
                0x6600, 0xA6C1, 0xA781, 0x6740, 0xA501, 0x65C0, 0x6480, 0xA441,
                0x6C00, 0xACC1, 0xAD81, 0x6D40, 0xAF01, 0x6FC0, 0x6E80, 0xAE41,
                0xAA01, 0x6AC0, 0x6B80, 0xAB41, 0x6900, 0xA9C1, 0xA881, 0x6840,
                0x7800, 0xB8C1, 0xB981, 0x7940, 0xBB01, 0x7BC0, 0x7A80, 0xBA41,
                0xBE01, 0x7EC0, 0x7F80, 0xBF41, 0x7D00, 0xBDC1, 0xBC81, 0x7C40,
                0xB401, 0x74C0, 0x7580, 0xB541, 0x7700, 0xB7C1, 0xB681, 0x7640,
                0x7200, 0xB2C1, 0xB381, 0x7340, 0xB101, 0x71C0, 0x7080, 0xB041,
                0x5000, 0x90C1, 0x9181, 0x5140, 0x9301, 0x53C0, 0x5280, 0x9241,
                0x9601, 0x56C0, 0x5780, 0x9741, 0x5500, 0x95C1, 0x9481, 0x5440,
                0x9C01, 0x5CC0, 0x5D80, 0x9D41, 0x5F00, 0x9FC1, 0x9E81, 0x5E40,
                0x5A00, 0x9AC1, 0x9B81, 0x5B40, 0x9901, 0x59C0, 0x5880, 0x9841,
                0x8801, 0x48C0, 0x4980, 0x8941, 0x4B00, 0x8BC1, 0x8A81, 0x4A40,
                0x4E00, 0x8EC1, 0x8F81, 0x4F40, 0x8D01, 0x4DC0, 0x4C80, 0x8C41,
                0x4400, 0x84C1, 0x8581, 0x4540, 0x8701, 0x47C0, 0x4680, 0x8641,
                0x8201, 0x42C0, 0x4380, 0x8341, 0x4100, 0x81C1, 0x8081, 0x4040,
        };


        byte[] bytes = data;
        int crc = 0xff;
        for (byte b : bytes) {
            crc = (crc >>> 8) ^ table[(crc ^ b) & 0xff];
        }

        System.out.println("CRC16 = " + Integer.toHexString(crc) + ", crc16=" + crc);

        return crc;
    }

    public static int crc16_update(int crc, byte a) {
        crc ^= ((a+128) & 0xff);
        for (int i = 0; i < 8; ++i) {
            if ((crc & 1) != 0) {
                crc = ((crc >>> 1) ^ 0xA001) & 0xffff;
            }
            else {
                crc = (crc >>> 1) & 0xffff;
            }
        }
        return crc;
    }

    public static short crc16Again(byte[] bytes) {
        int crc = 0;
        for (byte b : bytes) {
            crc = crc16_update(crc, b);
        }

        System.out.println(Integer.toHexString(crc));
        return (short) crc;
    }


    static int crc16(final byte[] buffer) {
        int crc = 0xFFFF;

        int j = 0;
        for (; j < buffer.length ; j++) {
//            System.out.println(Integer.toHexString(crc));
            crc = ((crc  >>> 8) | (crc  << 8) )& 0xffff;
//            System.out.println(Integer.toHexString(crc));
            crc ^= (buffer[j] & 0xff);//byte to int, trunc sign
            crc ^= ((crc & 0xff) >> 4);
            crc ^= (crc << 12) & 0xffff;
            crc ^= ((crc & 0xFF) << 5) & 0xffff;
        }

        System.out.println(buffer[j-1]);

        crc &= 0xffff;
        System.out.println("CRC16 = " + Integer.toHexString(crc) + ", crc16=" + crc);


        return crc;

    }

    @Test
    public void userDir() {
        System.out.println(System.getProperty("user.dir"));
    }

    @Test
    public void crc32() {
//        byte[] data = read("/home/fei/Desktop/ftp.txt", false);
        byte[] data = read(System.getProperty("user.dir") + "/packet001", false);

        CRC32 crc32 = new CRC32();
        crc32.update(data);

        System.out.println(Long.toHexString(crc32.getValue()));


    }

    @Test
    public void crc16() {
        readAllFileStream(System.getProperty("user.dir"), false)
                .filter(file -> file.getPath().contains("packet"))
                .sorted(Comparator.comparing(File::getName))
                .forEach(file -> {

//                    FtpSender.sendFile(file.getPath(), "v1.2.1/" + file.getName(), false);

                    System.out.println(file.getPath());
                    System.out.println(file.getName());

                    byte[] data = read(file.getPath(), false);

                    System.out.println(byteArrayToHexString(data));


                    ByteBuffer byteBuffer = ByteBuffer.wrap(data);

                    byte[] dst = new byte[1022];
                    byteBuffer.get(dst, 0, 1020);
//                    crc16(dst);

                    CRC16 crc16 = new CRC16();
                    for (int i = 0; i < 1020; i++) {
                        crc16.update(dst[i]);
                    }
                    System.out.println(" crc16 = " + crc16.value + ", hex=" + Integer.toHexString(crc16.value));



//                    byte[] bx = new byte[data.length - 6];
//                    for(int i = 0; i < bx.length; i++) bx[i] = data[i];
//                    System.out.println(crc16(bx));
//                    crc16Algorothim(bx);

                    byteBuffer.position(data.length - 4);

                    int crcCode = (int) byteBuffer.getChar();
                    int length = (int) byteBuffer.getChar();

                    log.info(String.format("packet length #%d, with crcCode=%d", length, crcCode));

                });
    }

    public static String byteArrayToHexString(byte[] b) {
        StringBuffer sb = new StringBuffer(b.length * 2);
        for (int i = 0; i < b.length; i++) {
            int v = b[i] & 0xff;
            if (v < 16) {
                sb.append('0');
            }
            sb.append(Integer.toHexString(v));
        }
        return sb.toString().toUpperCase();
    }

    @Test
    public void splitAndUploadOneByOne() {
        FtpSender.configure(new FtpEnvConfig("test.bimayun.com", "pwftp", "Qwerty555", FtpFileType.BINARY, "v1.2.4"));

        readAllFileStream(System.getProperty("user.dir"), false)
                .filter(file -> file.getPath().contains("packet"))
                .sorted(Comparator.comparing(File::getName))
                .forEach(file -> {

                    FtpSender.sendFile(file.getPath(), "v1.2.2/" + file.getName(), false);

                    System.out.println(file.getPath());
                    System.out.println(file.getName());

                    byte[] data = read(file.getPath(), false);

                    ByteBuffer byteBuffer = ByteBuffer.wrap(data);
                    byteBuffer.position(data.length - 4);

                    int crcCode = (int) byteBuffer.getChar();
                    int length = (int) byteBuffer.getChar();

                    log.info(String.format("packet length #%d, with crcCode=%d", length, crcCode));

                });
    }

    /**
     *  分割文件到默认文件夹
     *  每个文件添加 crc16 码
     *  上传到远程 ftp 服务器
     */
    @Test
    public void splitAndUploadBatch() {
        FtpSender.configure(new FtpEnvConfig("test.bimayun.com", "pwftp", "Qwerty555", FtpFileType.BINARY, "v1.2.4"));

        String originFile = "tcu_bin/TCU.bin";
        FileSplitterUtil.setSpiltSize(1020);
        FileSplitterUtil.splitAndCrc16(originFile);

        int crc16Code = FileSplitterUtil.crc16(originFile, true);
        System.out.printf("CRC16 code of %s is %d", originFile, crc16Code);

        Stream<File> fileStream = readAllFileStream(System.getProperty("user.dir"), false)
                .filter(file -> file.getName().startsWith("packet"))
                .sorted(Comparator.comparing(File::getName));
        FtpSender.batchSendFilesOneLevel(fileStream, "v1.2.3", false);

    }

    /**
     *  分割文件到指定文件夹
     *  每个文件添加 crc16 码
     *  上传到远程 ftp 服务器
     */
    @Test
    public void splitWithPrependPathAndUploadBatch() {
        FtpSender.configure(new FtpEnvConfig("test.bimayun.com", "pwftp", "Qwerty555", FtpFileType.BINARY, "v1.2.4"));

//        String originFile = "tcu_bin/TCUv1-2-0.bin";
        String originFile = "tcu_bin/TCU.bin";
        FileSplitterUtil.setSpiltSize(1020);
        FileSplitterUtil.splitAndCrc16(originFile, "packets", true);

        int crc16Code = FileSplitterUtil.crc16(originFile, true);
        System.out.printf("CRC16 code of %s is %d", originFile, crc16Code);

        Stream<File> fileStream = readAllFileStream(System.getProperty("user.dir") + File.separator + "packets", false)
                .filter(file -> file.getName().startsWith("packet"))
                .sorted(Comparator.comparing(File::getName));
        FtpSender.batchSendFilesOneLevel(fileStream, "v1.2.4", false);

    }
}
