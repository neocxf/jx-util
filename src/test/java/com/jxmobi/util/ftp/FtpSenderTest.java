package com.jxmobi.util.ftp;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.jxmobi.util.ftp.FtpSender.sendFile;

/**
 * @author Xiaofei Chen <a href="mailto:xchen@jxmobi.com">Email the author</a>
 * @version 1.0 5/29/16
 */
public class FtpSenderTest {
    private static final Logger log = LoggerFactory.getLogger(FtpSenderTest.class);

    @Test
    public void uploadOneFileToFtp() {
        FtpSender.configure(new FtpEnvConfig("test.bimayun.com", "pwftp", "Qwerty555", FtpFileType.BINARY));

        sendFile("tcu_bin/TCU.bin", "v1.2.1/TCUv1-2-0.bin", true);

    }
}
