package com.jxmobi.util;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.jxmobi.util.RegexUtils.*;

/**
 * @author Xiaofei Chen <a href="mailto:xchen@jxmobi.com">Email the author</a>
 * @version 1.0 4/12/2016
 */
public class RegexUtilsTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(RegexUtilsTest.class);

    @Test
    public void codeNumTest() {
        String regex = "1|2|3";

        System.out.println("1".matches(regex));
        System.out.println("2".matches(regex));
        System.out.println("3".matches(regex));
        System.out.println("4".matches(regex));
        System.out.println("-1".matches(regex));

    }

    @Test
    public void chineseTest() {
        String search = "测试";
//        Pattern pattern = Pattern.compile("(\\w+?)(:|<|>)(\\w+?|\\d{4}-\\d{1,2}-\\d{1,2}),");
        Pattern pattern = Pattern.compile("[\\u4E00-\\u9FA5]+");

        Matcher matcher = pattern.matcher(search);
        System.out.println(matcher.matches());
    }

    @Test
    public void searchParamTest() {
        String search = "iccid:*abcd*,abcd>,defg:sd,date>2012-11-1,aa:测试,sdsd:sds,isnull:,notnull:!NULL";
//        Pattern pattern = Pattern.compile("(\\w+?)(:|<|>)(\\w+?|\\d{4}-\\d{1,2}-\\d{1,2}),");
//        Pattern pattern = Pattern.compile("(\\w+?)(:|<|>)(!?\\w+?|[\\u4E00-\\u9FA5]+?|((19|20)\\d\\d)-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])),");
        Pattern pattern = Pattern.compile("(\\w+?)(" + ":|<|>" + ")(\\p{Punct}?)(\\w+?|[\\u4E00-\\u9FA5]+?|((19|20)\\d\\d)-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01]))(\\p{Punct}?),");

        Matcher matcher = pattern.matcher(search + ",");
//        Matcher matcher = pattern.matcher(search);
        while (matcher.find()) {
            LOGGER.debug(" total group is " + matcher.groupCount());
            LOGGER.debug(matcher.group(1));
            LOGGER.debug(matcher.group(2));
            LOGGER.debug(matcher.group(3));
            LOGGER.debug(matcher.group(4));
            LOGGER.debug(matcher.group(9));
            LOGGER.debug(matcher.group(6));
            LOGGER.debug(matcher.group(7));
            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<");
        }
    }


    @Test
    public void enhancedSearchParamTest() {
//        String search = "iccid:*abcd*,abcd!2,defg:sd,date>2012-11-1,aa:测试,sdsd:sds,isnull:,notnull:!NULL";
//        String search = "softVersion!avc";
        String search = "softVersion!V01-01-02";
//        Pattern pattern = Pattern.compile("(\\w+?)(:|<|>)(\\w+?|\\d{4}-\\d{1,2}-\\d{1,2}),");
//        Pattern pattern = Pattern.compile("(\\w+?)(:|<|>)(!?\\w+?|[\\u4E00-\\u9FA5]+?|((19|20)\\d\\d)-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])),");
        Pattern pattern = Pattern.compile("(\\w+?)(:|<|>|~|!)(\\p{Punct}?)([\\w|-]+?|[\\u4E00-\\u9FA5]+?|((19|20)\\d\\d)-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01]))(\\p{Punct}?),");

        Matcher matcher = pattern.matcher(search + ",");
//        Matcher matcher = pattern.matcher(search);
        while (matcher.find()) {
            LOGGER.debug(" total group is " + matcher.groupCount());
            LOGGER.debug(matcher.group(1));
            LOGGER.debug(matcher.group(2));
            LOGGER.debug(matcher.group(3));
            LOGGER.debug(matcher.group(4));
            LOGGER.debug(matcher.group(9));
            LOGGER.debug(matcher.group(6));
            LOGGER.debug(matcher.group(7));
            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<");
        }
    }

    @Test
    public void testDateRegex2() {
        Assert.assertTrue(dateRegex("2010-1-1"));
        Assert.assertTrue(dateRegex("2010-2-31"));
        Assert.assertTrue(!dateRegex("2010-13-1"));
        Assert.assertTrue(!dateRegex("2010-21-1"));
        Assert.assertTrue(!dateRegex("2010-21-32"));
        Assert.assertTrue(!dateRegex("2010-21-102"));
        Assert.assertTrue(!dateRegex("3010-21-102"));
    }

    @Test
    public void greaterThanTest() {
        String regex = "\\w{3,}";
        Assert.assertTrue("ad3abc".matches(regex));
    }

    @Test
    public void test2() {
//        String regex = "^[a-zA-z]\\w{2,9}]";
        String regex = "^[a-zA-z]\\w{2,10}";
        Assert.assertTrue("a1c".matches(regex));
    }

}
