package com.jxmobi.util;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * @author Xiaofei Chen <a href="mailto:xchen@jxmobi.com">Email the author</a>
 * @version 1.0 4/1/2016
 */
public class TimeUtilsTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(TimeUtilsTest.class);

    @Test
    public void timingTest() {
        for (int i = 3; i < 8; i++) {
            int size = (int) Math.pow(10, i);
            System.out.printf(" sorting array of length %,d.%n", size);
//            TimeUtils.timeOp(() -> sortArray(size));
            TimeUtils.timeOpConsumed(() -> sortArray(size));
        }
    }

    public static double[] randomNums(int length) {
        double[] nums = new double[length];
        for (int i = 0; i < length; i++) {
            nums[i] = Math.random();
        }
        return nums;
    }

    public static void sortArray(int length) {
        double[] nums = randomNums(length);
        Arrays.sort(nums);
    }

    @Test
    public void timezoneTest() {
        Date date = new Date();
        System.out.println(date);

        System.out.println(TimeZone.getDefault());
        System.out.println(Locale.getDefault());
    }
}
