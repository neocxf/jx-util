package com.jxmobi.util.algorithm;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * @author fei <a href="mailto:xchen@jxmobi.com">Email the author</a>
 * @version 1.0 7/22/16
 */
public class ByteSearchTest {
    private static final Logger log = LoggerFactory.getLogger(ByteSearchTest.class);


    @Test
    public void searchTest1() throws IOException, InterruptedException {
        BinarySearcher binarySearcher = new BigBinarySearcher();


        byte[] originBytes = Files.readAllBytes(Paths.get("TCUV01-02-08(1).bin"));

        long currentTime = System.currentTimeMillis();

        List<Integer> integerList = binarySearcher.searchBytes(originBytes, "V01-02-08".getBytes());

        long endTime = System.currentTimeMillis();

        System.out.println(integerList.size() + ", consume: {} " + (endTime - currentTime));

        Assert.assertNotNull(integerList);


    }
}
