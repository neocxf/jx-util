package com.jxmobi.util;

import com.jxmobi.util.file.ZipUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.JarURLConnection;
import java.nio.file.FileStore;
import java.nio.file.FileSystem;

/**
 * @author Xiaofei Chen <a href="mailto:xchen@jxmobi.com">Email the author</a>
 * @version 1.0 3/23/2016
 */
public class ZipUtilsTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(ZipUtilsTest.class);

//    @Test
    public void createZipFileSystemTest() throws IOException {
        System.out.println( );
        JarURLConnection con;
        FileSystem fs = ZipUtils.createZipFileSystem("readme.zip", true);
        Iterable<FileStore> fileStores = fs.getFileStores();
        fileStores.forEach((store) -> System.out.println(store.name()));
    }

//    @Test
    public void create() throws IOException {
        ZipUtils.create("readme.zip", "tbox.log");
    }

//    @Test
    public void unzip() throws IOException {
        ZipUtils.unzip("readme.zip", "test0");
    }
}
