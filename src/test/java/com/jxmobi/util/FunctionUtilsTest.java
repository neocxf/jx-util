package com.jxmobi.util;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

/**
 * @author Xiaofei Chen <a href="mailto:xchen@jxmobi.com">Email the author</a>
 * @version 1.0 4/2/2016
 */
public class FunctionUtilsTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(FunctionUtilsTest.class);

    @Test
    public void test() {
        List<Double> nums = Arrays.asList(2.0, 3.1, 1.9, 4.5);
        System.out.printf("original nums: %s.%n", nums);

        Function<Double, Double> square = x -> x * x;
        Function<Double, Double> half = x -> x / 3;
        System.out.printf("original nums: %s.%n", FunctionUtils.transform(2.1, half.compose(square)));
        System.out.printf("original nums: %s.%n", FunctionUtils.transform2(2.1, square, half));

    }
}
