package com.jxmobi.util;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

/**
 *   // 正则表达式，详情参见： http://zhidao.baidu.com/question/24445375.html?si=3
 * @author Xiaofei Chen <a href="mailto:xchen@jxmobi.com">Email the author</a>
 * @version 1.0 4/1/2016
 */
public class StringUtilsTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(StringUtilsTest.class);

    /**
     *  test for Lambada method reference
     */
    @Test
    public void test() {
        String s = "test";

        // SomeClass::staticMethod
        String result1 = StringUtils.transform(s, StringUtils::makeExciting);
        System.out.println(result1);

        // someObject::instanceMethod
        String prefix = "Blah";
        String result2 = StringUtils.transform(s, prefix::concat);
        System.out.println(result2);

        // SomeClass:instanceMethod, will populate an extra object
        String result3 = StringUtils.transform(s, String::toUpperCase);
        System.out.println(result3);
    }

    @Test
    public void divideTwo() {
        String uuid = "1509280011";
        String[] info = StringUtils.divideTwo(uuid, 6);
        Arrays.stream(info).forEach(System.out::println);

        int num = Integer.parseInt(info[1]);
        System.out.println(num);
    }
}
