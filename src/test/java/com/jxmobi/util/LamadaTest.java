package com.jxmobi.util;

import com.jxmobi.util.exception.IllegalOperationException;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * @author Xiaofei Chen <a href="mailto:xchen@jxmobi.com">Email the author</a>
 * @version 1.0 3/31/2016
 */
public class LamadaTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(LamadaTest.class);

    @Test
    public void breakTest() {
        String var = "a";

        switch (var) {
            case "a":
                if (2 > 1) {
                    System.out.println("2>1");
                    break;
                }
                System.out.println("outside the checkpoint");
                throw new IllegalOperationException("operation illegal");
            case "b":
                break;
        }

        System.out.println("hello break");
    }


    /**
     *  求集合中元素之和
     */
    @Test
    public void mapSum() {
        List<Integer> numbers = Arrays.asList(1,2,3,4,6,9,7,8,0);

        System.out.println(
                numbers.stream().
                        map(e -> e * 2)   // 对每个元素的处理
                        .reduce(0, (c, e) -> c + e) // 0 为初始值， c 为即将 return 的结果，e 为 map 的结果
        );

        // partitioningBy
        // only have two keys in the map: true or false
        Map<Boolean, List<Integer>> numMap = numbers.stream()
                .collect(Collectors.partitioningBy(e -> e % 2 == 0));

        System.out.println(numMap.get(true));
        System.out.println(numMap.get(false));

        // groupBy
        Map<Integer, List<Integer>> numGroup = numbers.stream()
                .collect(Collectors.groupingBy(e -> e % 2 ));

        System.out.println(numGroup.get(0));
        System.out.println(numGroup.get(1));

        // sum
        System.out.println(numbers.stream().parallel().reduce((e,r) -> e + r).get());
        System.out.println(numbers.stream().parallel().mapToInt(e -> e).sum());


    }

    /**
     *  functional composition
     *  seprate of concern
     */
    @Test
    public void findOvenAndDouble() {
        List<Integer> numbers = Arrays.asList(1,2,3,4,5,6,7,8,9);

        System.out.println(
                numbers.stream()
                        .filter(e -> e > 3)
                        .filter(e -> e % 2 == 0)
                        .map(e -> e * 2)
                        .findFirst()
        );
    }

    @Test
    public void testListAssigin() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);

        list.forEach(System.out::println);

        List<Integer> list1 = list;
        list1.forEach(System.out::println);
    }
}
