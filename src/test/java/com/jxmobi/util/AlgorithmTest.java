package com.jxmobi.util;

import com.jxmobi.util.algorithm.AlgorithmUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.zip.CRC32;
import java.util.zip.Checksum;

/**
 * @author Xiaofei Chen <a href="mailto:xchen@jxmobi.com">Email the author</a>
 * @version 1.0 5/25/16
 */
public class AlgorithmTest {
    private static final Logger log = LoggerFactory.getLogger(AlgorithmTest.class);

    @Test
    public void CalculateCRC32ChecksumForByteArray() {
        String input = "hello world";

        byte[] bytes = input.getBytes();

        Checksum checksum = new CRC32();

        checksum.update(bytes, 0, bytes
        .length);

        long checksumvalue =  checksum.getValue();

        log.info(String.format("CRC32 of %s is %d", input, checksumvalue));

    }

    @Test
    public void CRC16Test() {
        String input = "hello world";

        int result = AlgorithmUtils.crc16(input.getBytes());

        log.info(String.format("CRC32 of %s is %d", input, result));

    }

    @Test
    public void crc16_8005Test() {
        String input = "hello";

        int result = AlgorithmUtils.crc16_8005(input.getBytes());

        log.info(String.format("CRC32 of %s is %x", input, result));
    }
}
