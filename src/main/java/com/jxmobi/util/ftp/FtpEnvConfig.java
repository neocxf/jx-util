package com.jxmobi.util.ftp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  ftp 服务器连接信息
 * @author Xiaofei Chen <a href="mailto:xchen@jxmobi.com">Email the author</a>
 * @version 1.0 5/27/16
 */
public class FtpEnvConfig {
    private static final Logger log = LoggerFactory.getLogger(FtpEnvConfig.class);

    private String host;

    private int port;

    private String username;

    private String pass;

    private String path;

    private int fileType;

    public FtpEnvConfig() {

    }

    public FtpEnvConfig(String host, String username, String pass, FtpFileType fileType) {
        this(host, username, pass, fileType, "/");
        this.host = host;
    }

    public FtpEnvConfig(String host, String username, String pass, FtpFileType fileType, String path) {
        this(host, 21, username, pass, fileType, path);
    }

    public FtpEnvConfig(String host, int port, String username, String pass, FtpFileType fileType ,String path) {
        this.host = host;
        this.port = port;
        this.username = username;
        this.pass = pass;
        this.path = path;
        this.fileType = fileType.order();
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getFileType() {
        return fileType;
    }

    public void setFileType(FtpFileType fileType) {
        this.fileType = fileType.order();
    }

    @Override
    public String toString() {
        return "FtpEnvConfig{" +
                "host='" + host + '\'' +
                ", username='" + username + '\'' +
                ", pass='" + pass + '\'' +
                ", path='" + path + '\'' +
                ", fileType=" + fileType +
                '}';
    }
}
