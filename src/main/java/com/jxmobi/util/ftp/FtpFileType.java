package com.jxmobi.util.ftp;

/**
 *  ftp transport type, be complied with Apache commons-net's SocketClient FTP type
 *
 * @author Xiaofei Chen <a href="mailto:xchen@jxmobi.com">Email the author</a>
 * @version 1.0 5/27/16
 */
public enum FtpFileType {
    TEXT(1),
    BINARY(2);

    private int order;

    private FtpFileType(int order) {
        this.order = order;
    }

    public int order() {
        return this.order;
    }
}
