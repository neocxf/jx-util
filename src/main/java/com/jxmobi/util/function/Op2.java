package com.jxmobi.util.function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Xiaofei Chen <a href="mailto:xchen@jxmobi.com">Email the author</a>
 * @version 1.0 4/2/2016
 */
public interface Op2<T> {

    T runOp();

    static <T> T timeOp(Op2<T> operation) {
        T result;

        double ONE_BILLION = 1_000_000_000;
        long startTime = System.nanoTime();

        result = operation.runOp();

        long endTime = System.nanoTime();
        double elapsedSeconds = (endTime - startTime) / ONE_BILLION;
        System.out.printf(" Elapsed time:  %.3f seconds. %n", elapsedSeconds);

        return result;
    }


}
