package com.jxmobi.util.function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Xiaofei Chen <a href="mailto:xchen@jxmobi.com">Email the author</a>
 * @version 1.0 4/1/2016
 */
@FunctionalInterface
public interface StringFunction {
    String applyFunction(String s);
}
