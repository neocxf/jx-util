package com.jxmobi.util.function.tuple;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Xiaofei Chen <a href="mailto:xchen@jxmobi.com">Email the author</a>
 * @version 1.0 5/24/16
 */
public class Tuple1<T1> extends Tuple {
    private static final Logger log = LoggerFactory.getLogger(Tuple1.class);

    public final T1 t1;

    public Tuple1(int size, T1 t1) {
        super(size);
        this.t1 = t1;
    }

    public T1 getT1() {
        return t1;
    }

    @Override
    public Object get(int index) {
        return index == 0 ? t1 : null;
    }

    @Override
    public Object[] toArray() {
        return new Object[]{t1};
    }


}
