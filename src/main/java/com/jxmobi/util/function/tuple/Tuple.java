package com.jxmobi.util.function.tuple;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Iterator;

/**
 *  http://www.programcreek.com/java-api-examples/index.php?source_dir=reactor-master/reactor-core/src/main/java/reactor/fn/tuple/TupleN.java
 * @author Xiaofei Chen <a href="mailto:xchen@jxmobi.com">Email the author</a>
 * @version 1.0 5/24/16
 */
public abstract class Tuple implements Serializable, Iterable {
    private static final Logger log = LoggerFactory.getLogger(Tuple.class);
    private static final Object[] emptyArr = new Object[0];
//    private static final Tuple empty = new Tuple(0);

    protected final int size;

    public Tuple(int size) {
        this.size = size;
    }

    public static Tuple empty() {
        return null;
    }



    @Override
    public Iterator iterator() {
        return null;
    }

    public abstract Object get(int index);

    public abstract Object[] toArray();
}
