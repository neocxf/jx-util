package com.jxmobi.util.function;

/**
 * @author Xiaofei Chen <a href="mailto:xchen@jxmobi.com">Email the author</a>
 * @version 1.0 4/1/2016
 */
@FunctionalInterface
public interface Op {
    /**
     *  doing some stuff
     */
    void runOp();

    /**
     *  calculate the time to finish the operation
     *
     * @param operation
     */
    static void timeOp(Op operation) {
        double ONE_BILLION = 1_000_000_000;
        long startTime = System.nanoTime();

        operation.runOp();

        long endTime = System.nanoTime();
        double elapsedSeconds = (endTime - startTime) / ONE_BILLION;
        System.out.printf(" Elapsed time:  %.3f seconds. %n", elapsedSeconds);
    }


    /**
     *  combine the operation together and return as an new operation ,
     *  and then calculate the time consumed to finish all the operation
     *
     * @param secondOp
     * @return
     */
    default Op combineOp(Op secondOp) {
        return () -> {
            runOp();
            secondOp.runOp();
        };
    }
}
