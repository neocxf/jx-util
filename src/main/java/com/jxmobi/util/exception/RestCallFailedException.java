package com.jxmobi.util.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  suit for the  Rest call status code: 40X
 * @author fei <a href="mailto:xchen@jxmobi.com">Email the author</a>
 * @version 1.0 6/8/16
 */
public class RestCallFailedException extends Exception {
    private static final Logger log = LoggerFactory.getLogger(RestCallFailedException.class);

    private int statusCode;

    public RestCallFailedException(int statusCode) {
        this.statusCode = statusCode;
    }

    public RestCallFailedException(String message, int statusCode) {
        super(message);
        this.statusCode = statusCode;
    }

    public RestCallFailedException(String message, Throwable cause, int statusCode) {
        super(message, cause);
        this.statusCode = statusCode;
    }

    @Override
    public String getMessage() {
        return "client response with error code: " + statusCode + ", details are: " + super.getMessage();
    }
}
