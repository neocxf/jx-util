package com.jxmobi.util.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author fei <a href="mailto:xchen@jxmobi.com">Email the author</a>
 * @version 1.0 6/6/16
 */
public class FTPTimeoutException extends FTPSendException {


    public FTPTimeoutException(String message) {
        super(message);
    }

}
