package com.jxmobi.util.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author fei <a href="mailto:xchen@jxmobi.com">Email the author</a>
 * @version 1.0 6/5/16
 */
public class FTPSendException extends RuntimeException {
    private static final Logger log = LoggerFactory.getLogger(FTPSendException.class);

    public FTPSendException(String message) {
        super(message);
    }

    public FTPSendException(Throwable cause) {
        super(cause);
    }

    public FTPSendException(String message, Throwable cause) {
        super(message, cause);
    }
}
