package com.jxmobi.util.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  the operation(or code call) doesn't comply to the designer's intention
 *
 * @author fei <a href="mailto:xchen@jxmobi.com">Email the author</a>
 * @version 1.0 6/7/16
 */
public class IllegalOperationException extends RuntimeException{
    private static final Logger log = LoggerFactory.getLogger(IllegalOperationException.class);

    public IllegalOperationException(String message) {
        super(message);
    }

    public IllegalOperationException(String message, Throwable cause) {
        super(message, cause);
    }
}
