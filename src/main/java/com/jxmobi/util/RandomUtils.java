package com.jxmobi.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

/**
 * @author Xiaofei Chen <a href="mailto:xchen@jxmobi.com">Email the author</a>
 * @version 1.0 4/1/2016
 */
public class RandomUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(RandomUtils.class);

    private static final Random r = new Random(System.currentTimeMillis());

    public static int randomInt(int range) {
        return r.nextInt(range);
    }

    /**
     *  random index of an array
     * @param array
     * @return
     */
    public static int randomIndex(Object[] array) {
        return randomInt(array.length);
    }

    /**
     *  随机的返回一个数组里面的任意一个元素
     * @param array
     * @param <T>
     * @return
     */
    public static <T> T randomElement(T[] array) {
        return array[randomIndex(array)];
    }

}
