//package com.jxmobi.util;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.context.MessageSource;
//import org.springframework.util.StringUtils;
//
//import javax.servlet.http.HttpServletRequest;
//import java.util.Locale;
//
///**
// * @author Xiaofei Chen <a href="mailto:neocxf@qq.com">Email the author</a>
// * @version 1.0 1/9/2016
// */
//public class I18NUtil {
//    private static final Logger LOGGER = LoggerFactory.getLogger(I18NUtil.class);
//    private static MessageSource messageSource = (MessageSource) SpringUtil.getApplicationContext().getBean("messageSource");
//    private static final Object[] objs = null;
//
//    /**
//     * 获取国际化信息
//     * @param key 对应键
//     * @param locale 语言
//     * @param objects 参数
//     * @return 国际化信息
//     * @author Joyce.Luo
//     * @date 2016-1-5 上午10:19:00
//     * @version V3.0
//     * @since Tomcat6.0,Jdk1.6
//     * @copyright Copyright (c) 2016
//     */
//    public static String getMessage(String key, Locale locale, Object...objects){
//        if (StringUtils.isEmpty(key)) {
//            return StringUtils.EMPTY;
//        }
//        if (null == locale) {
//            HttpServletRequest request = ServletActionContext.getRequest();
//            if (null == request) {
//                locale = Locale.ENGLISH;
//            } else {
//                String lang = CookieUtil.getCookieByName(request, "language").getValue();
//                if (StringUtils.isEmpty(lang)){
//                    locale = Locale.ENGLISH;
//                } else {
//                    locale = EnumLang.getLocale(lang);
//                }
//            }
//        }
//        logger.info("Spring I18 ---> key：[" + key + "]，lang：[" + locale.getLanguage() + "]，params：[" + objects + "]");
//        return messageSource.getMessage(key, objects, locale);
//    }
//
//    /**
//     * 获取国际化信息
//     * @param key 对应键
//     * @param locale 语言
//     * @return 国际化信息
//     * @author Joyce.Luo
//     * @date 2016-1-5 上午10:19:00
//     * @version V3.0
//     * @since Tomcat6.0,Jdk1.6
//     * @copyright Copyright (c) 2016
//     */
//    public static String getMessage(String key, Locale locale){
//        return getMessage(key, locale, objs);
//    }
//
//    /**
//     * 获取国际化信息
//     * @param key 对应键
//     * @param objects 参数
//     * @return 国际化信息
//     * @author Joyce.Luo
//     * @date 2016-1-5 上午10:19:00
//     * @version V3.0
//     * @since Tomcat6.0,Jdk1.6
//     * @copyright Copyright (c) 2016
//     */
//    public static String getMessage(String key, Object...objects){
//        return getMessage(key, null, objects);
//    }
//
//    /**
//     * 获取国际化信息
//     * @param key 对应键
//     * @param lang 语言
//     * @return 国际化信息
//     * @author Joyce.Luo
//     * @date 2016-1-6 上午11:30:03
//     * @version V3.0
//     * @since Tomcat6.0,Jdk1.6
//     * @copyright Copyright (c) 2016
//     */
//    public static String getMessage(String key, String lang){
//        return getMessage(key, EnumLang.getLocale(lang));
//    }
//
//    /**
//     * 获取国际化信息
//     * @param key 对应键
//     * @return 国际化信息
//     * @author Joyce.Luo
//     * @date 2016-1-5 上午10:19:00
//     * @version V3.0
//     * @since Tomcat6.0,Jdk1.6
//     * @copyright Copyright (c) 2016
//     */
//    public static String getMessage(String key){
//        return getMessage(key, null, objs);
//    }
//}
