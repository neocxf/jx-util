package com.jxmobi.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Xiaofei Chen <a href="mailto:xchen@jxmobi.com">Email the author</a>
 * @version 1.0 3/24/2016
 */
public class RegexUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(RegexUtils.class);

    public static final String DATE_REGEX = "^((19|20)\\d\\d)-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])$";

    public static void main(String[] args) {
        String reg = "\\d{6}";
        boolean matches = Pattern.compile(reg).matcher("1234561").matches();
        System.out.println(matches);
    }

    private RegexUtils(){}

    /**
     *  检查一个字符串是否是日期型字符串
     *      注： 默认的检测规则为 2010-10-20
     * @param str 待检测的字符串
     * @return true，如果是日期型字符串
     */
    public static boolean dateRegex(String str) {
        Pattern pattern = Pattern.compile("^((19|20)\\d\\d)-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])$");
        Matcher matcher = pattern.matcher(str);
        return matcher.find();
    }


}
