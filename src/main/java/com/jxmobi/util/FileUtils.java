package com.jxmobi.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Xiaofei Chen <a href="mailto:xchen@jxmobi.com">Email the author</a>
 * @version 1.0 4/2/2016
 */
public class FileUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileUtils.class);

    private FileUtils(){}

    /**
     *  filter files and write to a new file
     * @param fileName
     * @param n
     * @param outFileName
     * @throws IOException
     */
    public static void filterFiles(String fileName, int n, String outFileName) throws IOException {
        List<String> words = Files.lines(Paths.get(fileName))
                .filter(s -> s.length() == n)
                .map(String::toUpperCase)
                .distinct()
                .sorted()
                .collect(Collectors.toList());

        Files.write(Paths.get(outFileName), words, Charset.forName("utf-8"));
    }
}
