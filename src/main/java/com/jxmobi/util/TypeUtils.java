package com.jxmobi.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  类型相关的工具类
 * @author Xiaofei Chen <a href="mailto:xchen@jxmobi.com">Email the author</a>
 * @version 1.0 4/20/2016
 */
public class TypeUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(TypeUtils.class);

    public static boolean isBoolean(Object obj) {
        if (obj instanceof Boolean)
            return true;

        if (obj instanceof String) {
            String str = (String) obj;
            return "true".equals(str) || "false".equals(str);
        }

        return false;
    }

    public static Boolean booleanValue(Object obj) {
        if (obj instanceof Boolean)
            return (Boolean) obj;
        return (obj instanceof String) ? Boolean.valueOf((String) obj) : null;
    }
}
