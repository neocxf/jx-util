package com.jxmobi.util.file.meta;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author fei <a href="mailto:xchen@jxmobi.com">Email the author</a>
 * @version 1.0 6/3/16
 */
public class SplitMeta {
    private int totalCrc16;

    private int totalPiece;

    public int getTotalCrc16() {
        return totalCrc16;
    }

    public void setTotalCrc16(int totalCrc16) {
        this.totalCrc16 = totalCrc16;
    }

    public int getTotalPiece() {
        return totalPiece;
    }

    public void setTotalPiece(int totalPiece) {
        this.totalPiece = totalPiece;
    }

    @Override
    public String toString() {
        return "SplitMeta{" +
                "totalCrc16=" + totalCrc16 +
                ", totalPiece=" + totalPiece +
                '}';
    }
}
