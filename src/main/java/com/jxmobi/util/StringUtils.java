package com.jxmobi.util;

import com.jxmobi.util.function.StringFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

/**
 * @author Xiaofei Chen <a href="mailto:xchen@jxmobi.com">Email the author</a>
 * @version 1.0 4/1/2016
 */
public class StringUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(StringUtils.class);

    public static String transform(String s, StringFunction function) {
        return (function.applyFunction(s));
    }

    /**
     *  从给定的位数将给定的字符串一分为2
     * @param originStr 待分解的字符串
     * @param idx 分割起始的位, 如：分割6位，从第6开始
     * @return 长度为2的字符串数组
     */
    public static String[] divideTwo(String originStr , int idx) {
        Objects.requireNonNull(originStr);

        if (idx < 0 || idx > originStr.length()) {
            throw new IllegalArgumentException(String.format("参数不合法，idx必须在%d到%d之间", 0, originStr.length()));
        }

        return new String[]{originStr.substring(0, idx), originStr.substring(idx)};
    }

    public static boolean isEmpty(String str) {
        return str != null && (str.length() == 0);
    }


    public static String makeExciting(String s) {
        return (s + "!!");
    }

    private StringUtils(){}
}
