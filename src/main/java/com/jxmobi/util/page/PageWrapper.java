package com.jxmobi.util.page;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 *  使用 ObjectMapper 转换，Page<User> userPage = ((PageImplBean<User>)new ObjectMapper().readValue(json, new TypeReference<PageImplBean<User>>() {})).pageImpl();
 *
 *  转换 json 对象到 pojo 对象， 地址： http://www.jsonschema2pojo.org/
 *
 *  详情参见： http://stackoverflow.com/questions/14445515/jsonmappingexception-cannot-desierialize-java-object
 *  http://stackoverflow.com/questions/30974286/com-fasterxml-jackson-databind-jsonmappingexception-can-not-deserialize-instanc
 *
 * @author Xiaofei Chen <a href="mailto:neocxf@qq.com">Email the author</a>
 * @version 1.0 1/8/2016
 */
public class PageWrapper<T> extends PageImpl<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(PageWrapper.class);

    private int number;
    private int size;
    private int totalPages;
    private int numberOfElements;
    private long totalElements;
    private boolean previous;
    private boolean first;
    private boolean next;
    private boolean last;
    private Sort sort;

    public PageImpl<T> pageImpl() {
        return new PageImpl<T>(getContent(), new PageRequest(getNumber(),
                getSize(), getSort()), getTotalElements());
    }

    public PageWrapper(List<T> content, Pageable pageable, long total) {
        super(content, pageable, total);
    }

    public PageWrapper(List<T> content) {
        super(content);
    }

    public PageWrapper() {
        super(new ArrayList<T>());
    }

    @Override
    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    @Override
    public int getNumberOfElements() {
        return numberOfElements;
    }

    public void setNumberOfElements(int numberOfElements) {
        this.numberOfElements = numberOfElements;
    }

    @Override
    public long getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(long totalElements) {
        this.totalElements = totalElements;
    }

    public boolean isPrevious() {
        return previous;
    }

    public void setPrevious(boolean previous) {
        this.previous = previous;
    }

    @Override
    public boolean isFirst() {
        return first;
    }

    public void setFirst(boolean first) {
        this.first = first;
    }

    public boolean isNext() {
        return next;
    }

    public void setNext(boolean next) {
        this.next = next;
    }

    @Override
    public boolean isLast() {
        return last;
    }

    public void setLast(boolean last) {
        this.last = last;
    }

    @Override
    public Sort getSort() {
        return sort;
    }

    public void setSort(Sort sort) {
        this.sort = sort;
    }
}
