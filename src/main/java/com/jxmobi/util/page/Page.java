package com.jxmobi.util.page;

import java.util.function.Function;

/**
 * @author Xiaofei Chen <a href="mailto:xchen@jxmobi.com">Email the author</a>
 * @version 1.0 5/20/16
 */
public interface Page<T> extends Slice<T> {

    /*
    * Returns the number of total pages.
    *
    * @return the number of total pages
    */
    int getTotalPages();

    /**
     * Returns the total amount of elements.
     *
     * @return the total amount of elements
     */
    long getTotalElements();

    /**
     * Returns a new {@link Page} with the content of the current one mapped by the given {@link Function}.
     *
     * @param converter must not be {@literal null}.
     * @return a new {@link Page} with the content of the current one mapped by the given {@link Function}.
     * @since 1.10
     */
    <S> Page<S> map(Function<? super T, ? extends S> converter);
}
