package com.jxmobi.util;

import java.util.Locale;

/**
 * @author Xiaofei Chen <a href="mailto:neocxf@qq.com">Email the author</a>
 * @version 1.0 1/9/2016
 */
public enum EnumLang {
    ZH_CN("zh-CN", Locale.SIMPLIFIED_CHINESE),
    ZH_TW("zh-TW", Locale.TRADITIONAL_CHINESE),
    EN("en", Locale.ENGLISH),
    JA("ja", Locale.JAPAN);

    private String lang;
    private Locale locale;

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    private EnumLang(String lang, Locale locale) {
        this.lang = lang;
        this.locale = locale;
    }

    /**
     * 根据ID获取枚举值
     * @param lang ID
     * @return ID对应的枚举值
     * @author Joyce.Luo
     * @date 2015-5-8 上午11:02:45
     * @version V3.0
     * @since Tomcat6.0,Jdk1.6
     * @copyright Copyright (c) 2015
     */
    public static Locale getLocale(String lang) {
        for (EnumLang enumLang : EnumLang.values()) {
            if (enumLang.getLang().equals(lang)) {
                return enumLang.getLocale();
            }
        }
        return Locale.ENGLISH;
    }
}
